package com.guido.enums;

/**
 *
 * @author Guido Thomassen
 */
public enum UserRole {
    NORMAL,
    MODERATOR
}

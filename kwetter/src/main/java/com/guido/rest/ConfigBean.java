/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guido.rest;

import com.guido.models.TwitterUser;
import com.guido.util.GenerateData;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * This bean is used for testing only, inserts data in db.
 * @author Guido Thomassen
 */
@Singleton
@Startup
public class ConfigBean {
    
    @PersistenceContext
    private EntityManager em;
    private static final Logger logger = Logger.getLogger(ConfigBean.class.getName());
    
    GenerateData gd = new GenerateData();
    
    @PostConstruct
    public void init() {
        
        TwitterUser user1 = gd.generateUser();
        TwitterUser user2 = gd.generateUser();
        TwitterUser user3 = gd.generateUser();
        
        user1.getFollowing().add(user1);
        user1.getFollowing().add(user2);
        user1.getFollowing().add(user3);
        
        user2.getFollowing().add(user2);
        user2.getFollowing().add(user1);
        user2.getFollowing().add(user3);
        
        user3.getFollowing().add(user3);
        user3.getFollowing().add(user2);
        
        em.persist(user1);
        em.persist(user2);
        em.persist(user3);
        
        logger.info("Inserted fake data into database");
    }
}

package com.guido.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Guido Thomassen
 */
@ApplicationPath("/resources")
public class Rest extends Application {
    
}

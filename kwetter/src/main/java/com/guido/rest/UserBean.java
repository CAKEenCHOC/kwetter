package com.guido.rest;

import com.guido.models.Tweet;
import com.guido.models.TwitterUser;
import com.guido.services.UserService;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Guido Thomassen
 */
@Stateless
@Named
@Path("/user")
public class UserBean {
    
    @Inject
    UserService userService;
    
    private static final Logger logger = Logger.getLogger(ConfigBean.class.getName());
    
    @POST
    @Path("/changeUsername/{twitterUserID}/{username}")
    @Produces(MediaType.APPLICATION_JSON)
    public TwitterUser changeUsername(@PathParam("twitterUserID") Long twitterUserID,
            @PathParam("username") String username) {
        
        logger.log(Level.INFO, "Requesting new username for {0} to {1}", 
                new Object[]{twitterUserID, username});
        return userService.changeUsername(twitterUserID, username);
    }
    
    @POST
    @Path("/changeDescription/{twitterUserID}/{description}")
    @Produces(MediaType.APPLICATION_JSON)
    public TwitterUser changeDescription(@PathParam("twitterUserID") Long twitterUserID,
            @PathParam("description") String description) {
        
        logger.log(Level.INFO, "Requesting new description for {0} to {1}", 
                new Object[]{twitterUserID, description});
        return userService.changeDescription(twitterUserID, description);
    }
    
    @POST
    @Path("/changeLocation/{twitterUserID}/{location}")
    @Produces(MediaType.APPLICATION_JSON)
    public TwitterUser changeLocation(@PathParam("twitterUserID") Long twitterUserID,
            @PathParam("location") String location) {
        
        logger.log(Level.INFO, "Requesting new location for {0} to {1}", 
                new Object[]{twitterUserID, location});
        return userService.changeLocation(twitterUserID, location);
    }
    
    @POST
    @Path("/changeWebsite/{twitterUserID}/{website}")
    @Produces(MediaType.APPLICATION_JSON)
    public TwitterUser changeWebsite(@PathParam("twitterUserID") Long twitterUserID,
            @PathParam("website") String website) {
        
        logger.log(Level.INFO, "Requesting new website for {0} to {1}", 
                new Object[]{twitterUserID, website});
        return userService.changeWebsite(twitterUserID, website);
    }
    
    @GET
    @Path("/getFollowers/{twitterUserID}")
    public Set<TwitterUser> getFollowers(@PathParam("twitterUserID") Long twitterUserID) {
        logger.log(Level.INFO, "Requesting followers for {0}", twitterUserID);
        return userService.getFollowers(twitterUserID);
    }
    
    @GET
    @Path("/getFollowing/{twitterUserID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<TwitterUser> getFollowing(@PathParam("twitterUserID") Long twitterUserID) {
        logger.log(Level.INFO, "Requesting following for {0}", twitterUserID);
        return userService.getFollowing(twitterUserID);
    }
    
    @GET
    @Path("/generateTimeline/{twitterUserID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Tweet> generateTimeline(@PathParam("twitterUserID") Long twitterUserID) {
        logger.log(Level.INFO, "Requesting timeline for {0}", twitterUserID);
        return userService.generateTimeline(twitterUserID);
    }
}

package com.guido.rest;

import com.guido.dao.interfaces.TweetDao;
import com.guido.models.Tweet;
import com.guido.services.TweetService;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Guido Thomassen
 */
@Stateless
@Path("/tweet")
public class TweetBean {
    
    @Inject
    TweetService tweetService;
    
    private static final Logger logger = Logger.getLogger(TweetBean.class.getName());
    
    @GET
    @Path("/getLastTweets/{amount}/{userID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Tweet> getLastTweets(@PathParam("amount") int amount, 
           @PathParam("userID") Long userID) {
        
        logger.log(Level.INFO, "User with ID {0} asking for last {1} tweets", 
                new Object[]{userID, amount});

        return tweetService.getLastTweets(amount, userID);
    }
    
    @POST
    @Path("/retweet/{tweetID}/{userID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Tweet retweet(@PathParam("tweetID") Long tweetID, 
            @PathParam("userID") Long userID) {
        logger.log(Level.INFO, "User with ID {0} asking for retweet {1}", 
                new Object[]{userID, tweetID});
        
        return tweetService.retweet(tweetID, userID);
    }
    
    @GET
    @Path("/getTweetsWithCriteria/{criteria}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Tweet> getTweetsWithCriteria(@PathParam("criteria") String criteria) {
        logger.log(Level.INFO, "Searching for tweets with {0}", criteria);
        
        return tweetService.getTweetWithCriteria(criteria);
    }
    
    @POST
    @Path("/publishTweet/{twitterUserID}/{content}")
    @Produces(MediaType.APPLICATION_JSON)
    public Tweet publishTweet(@PathParam("twitterUserID") Long twitterUserID,
            @PathParam("content") String content) {
        
        logger.log(Level.INFO, "Creating new tweet for user with ID {0}", twitterUserID);
        
        return tweetService.publishTweet(twitterUserID, content);
    }
}

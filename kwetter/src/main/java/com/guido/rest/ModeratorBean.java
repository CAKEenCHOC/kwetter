package com.guido.rest;

import com.guido.models.TwitterUser;
import com.guido.services.ModeratorService;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;

/**
 *
 * @author Guido Thomassen
 */
@Stateless
@Path("/moderator")
public class ModeratorBean {
    
    @Inject
    ModeratorService moderatorService;
    
    private static final Logger logger = Logger.getLogger(ModeratorBean.class.getName());
    
    @POST
    @Path("/changeUserToModerator/{userID}")
    @Produces(MediaType.APPLICATION_JSON)
    public TwitterUser changeUserToModerator(@PathParam("userID") Long userID) {
        logger.log(Level.INFO, "Moderator asking to update {0} to moderator.", userID);
        
        return moderatorService.changeUserToModerator(userID);
    }
    
    @POST
    @Path("/changeModeratorToUser/{userID}")
    @Produces(MediaType.APPLICATION_JSON)
    public TwitterUser changeModeratorToUser(@PathParam("userID") Long userID) {
        logger.log(Level.INFO, "Moderator asking to update {0} to normal user.", userID);
        
        return moderatorService.changeModeratorToUser(userID);
    }
    
    @GET
    @Path("/getUsersWithRole")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<TwitterUser> getUsersWithRole() {
        logger.log(Level.INFO, "Moderator asking for users with role");
        
        return moderatorService.getUsersWithRole();
    }
    
    @POST
    @Path("/deleteTweet/{tweetID}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean deleteTweet(@PathParam("tweetID") Long tweetID) {
        logger.log(Level.INFO, "Moderator asking to delete tweet with ID {0}", tweetID);
        
        return moderatorService.deleteTweet(tweetID);
    }
}

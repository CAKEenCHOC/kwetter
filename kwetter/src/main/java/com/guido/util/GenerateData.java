package com.guido.util;

import com.guido.enums.UserRole;
import com.guido.models.Tweet;
import com.guido.models.TwitterUser;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author Guido Thomassen
 */
public class GenerateData {
    
    int currentUserID = 0;
    int currentTweetID = 0;
    
    Random random = new Random();
    
    public TwitterUser generateUser() {
        TwitterUser user = new TwitterUser();
        user.setId(getcurrentUserID());
        user.setUsername("Henkie040");
        user.setDescription("Hello my name is Henk and I like Kwetter.");
        user.setLocation("Eindhoven");
        user.setUserRole(UserRole.NORMAL);
        user.setWebsite("http://www.henkie.nl");
        
        Set<Tweet> tweets = new HashSet<>();
        user.setTweets(tweets);
        
        user.getTweets().add(generateTweet(user));
        user.getTweets().add(generateTweet(user));
        user.getTweets().add(generateTweet(user));

        Set<TwitterUser> following = new HashSet<>();
        user.setFollowing(following);
        
        return user;
    }
    
    public Tweet generateTweet(TwitterUser user) {
        Tweet tweet = new Tweet();
        tweet.setId(getCurrentTweet());
        tweet.setContent("This is a tweet with sample text");
        tweet.setUnixDate((System.currentTimeMillis() / 1000L) - random.nextInt(1000000));
        tweet.setOwner(user);
        return tweet;
    }
    
    private Long getcurrentUserID() {
        currentUserID++;
        return new Long(currentUserID);
    }
    
    private Long getCurrentTweet() {
        currentTweetID++;
        return new Long(currentTweetID);
    }
}

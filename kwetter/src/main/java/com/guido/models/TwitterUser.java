package com.guido.models;

import com.guido.enums.UserRole;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author Guido Thomassen
 */
@Entity
@NamedQuery(name="TwitterUser.findAll", query="SELECT t FROM TwitterUser t")  

public class TwitterUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(unique=true)
    private String username;
    
    private String description;
    
    private String location;
    
    private String website;
    
    private UserRole userRole;

    @OneToMany (mappedBy="owner", cascade = CascadeType.ALL)
    private Set<Tweet> tweets;
    
    @OneToMany (cascade = CascadeType.ALL)
    private Set<TwitterUser> following;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @JsonbTransient
    public Set<Tweet> getTweets() {
        return tweets;
    }

    public void setTweets(Set<Tweet> tweets) {
        this.tweets = tweets;
    }

    @JsonbTransient
    public Set<TwitterUser> getFollowing() {
        return following;
    }

    public void setFollowing(Set<TwitterUser> following) {
        this.following = following;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 7 * hash + Objects.hashCode(this.id);
        hash = 7 * hash + Objects.hashCode(this.username);
        hash = 7 * hash + Objects.hashCode(this.description);
        hash = 7 * hash + Objects.hashCode(this.location);
        hash = 7 * hash + Objects.hashCode(this.website);
        hash = 7 * hash + Objects.hashCode(this.userRole);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TwitterUser other = (TwitterUser) obj;
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.location, other.location)) {
            return false;
        }
        if (!Objects.equals(this.website, other.website)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (this.userRole != other.userRole) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.guido.TwitterUser[ id=" + id + " ]";
    }
    
}

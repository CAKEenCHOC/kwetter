package com.guido.models;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Guido Thomassen
 */
@Entity
public class Tweet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String content;
    
    @ManyToOne
    private TwitterUser owner;
    
    private Long unixDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public TwitterUser getOwner() {
        return owner;
    }

    public void setOwner(TwitterUser owner) {
        this.owner = owner;
        this.owner.getTweets().add(this);
    }

    public Long getUnixDate() {
        return unixDate;
    }

    public void setUnixDate(Long unixDate) {
        this.unixDate = unixDate;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        hash = 67 * hash + Objects.hashCode(this.content);
        hash = 67 * hash + Objects.hashCode(this.owner.getUsername());
        hash = 67 * hash + Objects.hashCode(this.unixDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tweet other = (Tweet) obj;
        if (!Objects.equals(this.content, other.content)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.owner, other.owner)) {
            return false;
        }
        if (!Objects.equals(this.unixDate, other.unixDate)) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        return "com.guido.models.Tweet[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guido.services;

import com.guido.dao.interfaces.ModeratorDao;
import com.guido.models.TwitterUser;
import java.util.Set;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Guido Thomassen
 */
@Stateless
public class ModeratorService {
    
    /**
     * ModeratorDao used for communication with the database.
     */
    @Inject ModeratorDao moderatorDao;

    /**
     * Change a user to a moderator.
     * @param userID The ID of the user. Can't be null.
     * @return Updated TwitterUser if DAO returned that the change was valid.
     */
    public TwitterUser changeUserToModerator(Long userID) {
        if (userID == null) {
            return null;
        }
        
        return moderatorDao.changeUserToModerator(userID);
    }

    /**
     * Change a moderator to a user.
     * @param userID The ID of the user. Can't be null.
     * @return Updated TwitterUser if DAO returned that the change was valid.
     */
    public TwitterUser changeModeratorToUser(Long userID) {
        if (userID == null) {
            return null;
        }
        
        return moderatorDao.changeModeratorToUser(userID);
    }

    /**
     * Get a list of users with their role.
     * @return A list of all users.
     */
    public Set<TwitterUser> getUsersWithRole() {
        return moderatorDao.getUsersWithRole();
    }

    /**
     * Delete a tweet.
     * @param tweetID The ID of the tweet. Can't be null.
     * @return True if deleted, false if not.
     */
    public boolean deleteTweet(Long tweetID) {
        if (tweetID == null) {
            return false;
        }
        
        return moderatorDao.deleteTweet(tweetID);
    }
    
    /**
     * This is used for testing only.
     * Sets the DAO.
     * @param moderatorDao The DAO to use for testing.
     */
    public void setDaoForTesting(ModeratorDao moderatorDao) {
        this.moderatorDao = moderatorDao;
    } 
}

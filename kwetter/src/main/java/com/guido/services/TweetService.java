/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guido.services;

import com.guido.dao.interfaces.TweetDao;
import com.guido.models.Tweet;
import java.util.Set;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Guido Thomassen
 */
@Stateless
public class TweetService {
    
    /**
     * DAO which is used for communication with the database.
     */
    @Inject TweetDao tweetDao;

    /**
     * Get the last X tweets from a user.
     * @param amount The amount of tweets, cant be negative or 0.
     * @param userID The ID of the user, can't be null.
     * @return The X last tweets from a user.
     */
    public Set<Tweet> getLastTweets(int amount, Long userID) {
        if (amount <= 0) {
            return null;
        }
        
        if (userID == null) {
            return null;
        }
        
        return tweetDao.getLastTweets(amount, userID);
    }

    /**
     * Retweets a tweet from a user.
     * @param tweetID The tweet to retweet, can't be null.
     * @param userID The ID of the user who wants to retweet, can't be null.
     * @return The tweet which was retweeted if succes, null if not.
     */
    public Tweet retweet(Long tweetID, Long userID) {
        if (tweetID == null) {
            return null;
        }
        
        if (userID == null) {
            return null;
        }
        
        return tweetDao.retweet(tweetID, userID);
    }

    /**
     * Search in tweets with a search criteria.
     * @param criteria The criteria to search for. Can't be null or empty.
     * @return A Set of tweets containing the criteria.
     */
    public Set<Tweet> getTweetWithCriteria(String criteria) {
        if (criteria == null || criteria.trim().isEmpty()) {
            return null;
        }
        
        return tweetDao.getTweetWithCriteria(criteria);
    }

    /**
     * Publish a tweet.
     * @param twitterUserID The ID of the user who wants to publish. Can't be null.
     * @param content The content of the tweet. Can't be null or empty.
     * @return 
     */
    public Tweet publishTweet(Long twitterUserID, String content) {
        if (twitterUserID == null) {
            return null;
        }
        
        if (content == null || content.trim().isEmpty()) {
            return null;
        }
        
        return tweetDao.publishTweet(twitterUserID, content);
    }
    
    /**
     * This is used for testing only.
     * Sets the DAO.
     * @param tweetDao The DAO to use for testing.
     */
    public void setDaoForTesting(TweetDao tweetDao) {
        this.tweetDao = tweetDao;
    }
}

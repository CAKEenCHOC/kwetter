/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guido.services;

import com.guido.dao.interfaces.UserDao;
import com.guido.models.Tweet;
import com.guido.models.TwitterUser;
import java.util.Set;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Guido Thomassen
 */
@Stateless
public class UserService {
    
    /**
     * The UserDAO used for communication with the database.
     */
    @Inject UserDao userDao;

    /**
     * Change the username of a user.
     * @param twitterUserID The ID of the user, can't be null.
     * @param username The new username, can't be null or empty.
     * @return The updated user if change was succes, null if not.
     */
    public TwitterUser changeUsername(Long twitterUserID, String username) {
        if (twitterUserID == null) {
            return null;
        }
        
        if (username == null || username.trim().isEmpty()) {
            return null;
        }
        
        return userDao.changeUsername(twitterUserID, username);
    }

    /**
     * Change the description of a user.
     * @param twitterUserID The ID of the user, can't be null.
     * @param description The new description, can't be null.
     * @return The updated user if change was succes, null if not.
     */
    public TwitterUser changeDescription(Long twitterUserID, String description) {
        if (twitterUserID == null) {
            return null;
        }
        
        if (description == null) {
            return null;
        }
        
        return userDao.changeDescription(twitterUserID, description);
    }

    /**
     * Change the location of a user.
     * @param twitterUserID The ID of the user, can't be null.
     * @param location The new location, can't be null.
     * @return The updated user if change was succes, null if not.
     */
    public TwitterUser changeLocation(Long twitterUserID, String location) {
        if (twitterUserID == null) {
            return null;
        }
        
        if (location == null) {
            return null;
        }
        
        return userDao.changeLocation(twitterUserID, location);
    }

    /**
     * Change the website of a user.
     * @param twitterUserID The ID of the user, can't be null.
     * @param website The new website, can't be null.
     * @return The updated user if change was succes, null if not.
     */
    public TwitterUser changeWebsite(Long twitterUserID, String website) {
        if (twitterUserID == null) {
            return null;
        }
        
        if (website == null) {
            return null;
        }
        
        return userDao.changeWebsite(twitterUserID, website);
    }

    /**
     * Get all the followers from a user.
     * @param twitterUserID The ID of the user, can't be null.
     * @return A Set containing all the followers of the user.
     */
    public Set<TwitterUser> getFollowers(Long twitterUserID) {
        if (twitterUserID == null) {
            return null;
        }
        
        return userDao.getFollowers(twitterUserID);
    }

    /**
     * Get all the users which are followed by a user.
     * @param twitterUserID The ID of the user, can't be null.
     * @return A set containing all the users who a user follows.
     */
    public Set<TwitterUser> getFollowing(Long twitterUserID) {
        if (twitterUserID == null) {
            return null;
        }
        
        return userDao.getFollowing(twitterUserID);
    }

    /**
     * Generates a timeline for a user.
     * @param twitterUserID The ID of the user, can't be null.
     * @return A Set of tweets containing the timeline.
     */
    public Set<Tweet> generateTimeline(Long twitterUserID) {
        if (twitterUserID == null) {
            return null;
        }
    
        return userDao.generateTimeline(twitterUserID);
    }
    
    /**
     * This is used for testing only.
     * Sets the DAO.
     * @param userDao The DAO to use for testing.
     */
    public void setDaoForTesting(UserDao userDao) {
        this.userDao = userDao;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guido.dao.implementations;

import com.guido.dao.interfaces.UserDao;
import com.guido.models.Tweet;
import com.guido.models.TwitterUser;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.ColumnResult;
import javax.persistence.EntityManager;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.SqlResultSetMapping;

/**
 *
 * @author Guido Thomassen
 */
@Stateless
public class UserDaoImpl implements UserDao {
    
    @PersistenceContext
    EntityManager em;

    @Override
    public TwitterUser changeUsername(Long twitterUserID, String username) {
        TwitterUser user = em.find(TwitterUser.class, twitterUserID);
        
        if (user == null) {
            return null;
        }
        
        user.setUsername(username);
        em.merge(user);
        
        return user;
    }

    @Override
    public TwitterUser changeDescription(Long twitterUserID, String description) {
        
        TwitterUser user = em.find(TwitterUser.class, twitterUserID);
        
        if (user == null) {
            return null;
        }
        
        user.setDescription(description);
        em.merge(user);
        
        return user;
    }

    @Override
    public TwitterUser changeLocation(Long twitterUserID, String location) {
        
        TwitterUser user = em.find(TwitterUser.class, twitterUserID);
        
        if (user == null) {
            return null;
        }
        
        user.setLocation(location);
        em.merge(user);
        
        return user;
    }

    @Override
    public TwitterUser changeWebsite(Long twitterUserID, String website) {
        
        TwitterUser user = em.find(TwitterUser.class, twitterUserID);
        
        if (user == null) {
            return null;
        }
        
        user.setWebsite(website);
        em.merge(user);
        
        return user;
    }

    @Override
    public Set<TwitterUser> getFollowers(Long twitterUserID) {
        TwitterUser user = em.find(TwitterUser.class, twitterUserID);
        
        if (user == null) {
            return null;
        }
        
        Query query = em.createNativeQuery("SELECT t.* FROM twitteruser t, twitteruser_twitteruser k WHERE k.following_id = " + twitterUserID + " AND t.id = k.twitteruser_id");
        return new HashSet<>((List<TwitterUser>)query.getResultList());
    }

    @Override
    public Set<TwitterUser> getFollowing(Long twitterUserID) {
        TwitterUser user = em.find(TwitterUser.class, twitterUserID);
        
        if (user == null) {
            return null;
        }
        
        return user.getFollowing();
    }

    @Override
    public Set<Tweet> generateTimeline(Long twitterUserID) {
        
        TwitterUser user = em.find(TwitterUser.class, twitterUserID);
        
        if (user == null) {
            return null;
        }
        
        Query query = em.createNativeQuery("SELECT t.* FROM Tweet t, "
                + "twitteruser_twitteruser k WHERE k.following_id = t.owner_id "
                + "AND k.twitteruser_id = " + twitterUserID +" AND k.following_id "
                + "IN (SELECT following_ID FROM twitteruser_twitteruser "
                + "WHERE twitteruser_id = " + twitterUserID + ") "
                + "ORDER BY unixdate DESC");
        
        return new HashSet<>((List<Tweet>)query.getResultList());
    }
    
    
}

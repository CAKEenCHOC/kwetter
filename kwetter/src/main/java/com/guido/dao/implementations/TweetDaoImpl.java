/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guido.dao.implementations;

import com.guido.dao.interfaces.TweetDao;
import com.guido.models.Tweet;
import com.guido.models.TwitterUser;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Guido Thomassen
 */
@Stateless
public class TweetDaoImpl implements TweetDao {
    
    @PersistenceContext
    EntityManager em;
    
    @Override
    public Set<Tweet> getLastTweets(int amount, Long userID) {
        
        TwitterUser user = (TwitterUser) em.find(TwitterUser.class, userID);
        
        if (user == null) {
            return null;
        }
        
        Set<Tweet> allTweets = user.getTweets();
        
        if (allTweets.size() <= amount) {
            return allTweets;
        } else {
            Set<Tweet> toReturn = new HashSet<>();
            Iterator it = allTweets.iterator();
        
            for (int i = 0; i < amount; i++) {
                toReturn.add((Tweet) it.next());
            }

            return toReturn;
        }
        
    }

    @Override
    public Tweet retweet(Long tweetID, Long userID) {
        TwitterUser user = (TwitterUser) em.find(TwitterUser.class, userID);
        
        if (user == null) {
            return null;
        }
        
        Tweet tweet = (Tweet) em.find(Tweet.class, tweetID);
        
        if (tweet == null) {
            return null;
        }
        
        if (user.equals(tweet.getOwner())) {
            return null;
        }
        
        if (user.getTweets().contains(tweet)) {
            return null;
        }
        
        user.getTweets().add(tweet);
        em.merge(user);
        
        return tweet;
    }

    @Override
    public Set<Tweet> getTweetWithCriteria(String criteria) {
        return new HashSet<>(em.createQuery("Select t from Tweet t WHERE "
                + "t.content LIKE '%" + criteria + "%'", Tweet.class).getResultList());
    }

    @Override
    public Tweet publishTweet(Long twitterUserID, String content) {
        TwitterUser twitterUser = em.find(TwitterUser.class, twitterUserID);
        
        if (twitterUser == null) {
            return null;
        }
        
        if (content == null || content.trim().isEmpty()) {
            return null;
        }
        
        Tweet tweet = new Tweet();
        tweet.setContent(content);
        tweet.setOwner(twitterUser);
        tweet.setUnixDate(System.currentTimeMillis() / 1000L);
        
        em.merge(twitterUser);
        
        return tweet;
    }
}

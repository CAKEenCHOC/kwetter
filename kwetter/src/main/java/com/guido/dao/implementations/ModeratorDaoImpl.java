/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guido.dao.implementations;

import com.guido.dao.interfaces.ModeratorDao;
import com.guido.enums.UserRole;
import com.guido.models.Tweet;
import com.guido.models.TwitterUser;
import java.util.HashSet;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Guido Thomassen
 */
@Stateless
public class ModeratorDaoImpl implements ModeratorDao {
    
    @PersistenceContext
    EntityManager em;

    @Override
    public TwitterUser changeUserToModerator(Long userID) {
        TwitterUser user = (TwitterUser) em.find(TwitterUser.class, userID);
        
        if (user == null) {
            return null;
        }
            
        user.setUserRole(UserRole.MODERATOR);
        em.merge(user);
        return user;
    }

    @Override
    public TwitterUser changeModeratorToUser(Long userID) {
        TwitterUser user = (TwitterUser) em.find(TwitterUser.class, userID);
        
        if (user == null) {
            return null;
        }
            
        user.setUserRole(UserRole.NORMAL);
        em.merge(user);
        return user;
    }

    @Override
    public Set<TwitterUser> getUsersWithRole() {
        return new HashSet<>(em.createNamedQuery("TwitterUser.findAll").getResultList());
    }

    @Override
    public boolean deleteTweet(Long tweetID) {
        Tweet tweet = (Tweet) em.find(Tweet.class, tweetID);
        
        if (tweet == null) {
            return false;
        }
        
        em.remove(tweet);
        return true;
    }
    
}

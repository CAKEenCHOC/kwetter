/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guido.dao.interfaces;

import com.guido.models.Tweet;
import com.guido.models.TwitterUser;
import java.util.Set;

/**
 *
 * @author Guido Thomassen
 */
public interface UserDao {
    
    public TwitterUser changeUsername(Long twitterUserID, String username);
    
    public TwitterUser changeDescription(Long twitterUserID, String description);
    
    public TwitterUser changeLocation(Long twitterUserID, String location);
    
    public TwitterUser changeWebsite(Long twitterUserID, String website);
    
    public Set<TwitterUser> getFollowers(Long twitterUserID);
    
    public Set<TwitterUser> getFollowing(Long twitterUserID);
    
    public Set<Tweet> generateTimeline(Long twitterUserID);
}

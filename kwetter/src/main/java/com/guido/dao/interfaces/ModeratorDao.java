/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guido.dao.interfaces;

import com.guido.models.TwitterUser;
import java.util.Set;

/**
 *
 * @author Guido Thomassen
 */
public interface ModeratorDao {
    
    public TwitterUser changeUserToModerator(Long userID);
    
    public TwitterUser changeModeratorToUser(Long userID);
    
    public Set<TwitterUser> getUsersWithRole();
    
    public boolean deleteTweet(Long tweetID);
}

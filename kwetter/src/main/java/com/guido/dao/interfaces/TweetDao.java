/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guido.dao.interfaces;

import com.guido.models.Tweet;
import java.util.Set;

/**
 *
 * @author Guido Thomassen
 */
public interface TweetDao {
    
    public Set<Tweet> getLastTweets(int amount, Long userID);
    
    public Tweet retweet(Long tweetID, Long userID);
    
    public Set<Tweet> getTweetWithCriteria(String criteria);
    
    public Tweet publishTweet(Long twitterUserID, String content);
}

package com.guido.models;

import com.guido.util.GenerateData;
import com.guido.enums.UserRole;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Guido Thomassen
 */
public class TwitterUserTest {
    
    private TwitterUser twitterUser;
    
    GenerateData gd = new GenerateData();
    
    @Before
    public void setup() {
        twitterUser = gd.generateUser();
    }

    @Test
    public void testGetId() {
        Assert.assertNotNull(twitterUser.getId());
    }

    @Test
    public void testSetId() {
        Long previousID = twitterUser.getId();
        twitterUser.setId(new Long(-10000));
        Assert.assertNotEquals(previousID, twitterUser.getId());
    }

    @Test
    public void testGetUsername() {
        Assert.assertNotNull(twitterUser.getUsername());
    }

    @Test
    public void testSetUsername() {
        String username = twitterUser.getUsername();
        twitterUser.setUsername("PIET");
        Assert.assertNotEquals(username, twitterUser.getUsername());
    }

    @Test
    public void testGetDescription() {
        Assert.assertNotNull(twitterUser.getDescription());
    }

    @Test
    public void testSetDescription() {
        String description = twitterUser.getDescription();
        twitterUser.setDescription("NEW DESCRIPTION");
        Assert.assertNotEquals(description, twitterUser.getDescription());
    }

    @Test
    public void testGetLocation() {
        Assert.assertNotNull(twitterUser.getLocation());
    }

    @Test
    public void testSetLocation() {
        String location = twitterUser.getLocation();
        twitterUser.setLocation("NEW LOCATION");
        Assert.assertNotEquals(location, twitterUser.getLocation());
    }

    @Test
    public void testGetWebsite() {
        Assert.assertNotNull(twitterUser.getWebsite());
    }

    @Test
    public void testSetWebsite() {
        String website = twitterUser.getWebsite();
        twitterUser.setWebsite("http://newwebsite.nl");
        Assert.assertNotEquals(website, twitterUser.getWebsite());
    }

    @Test
    public void testGetTweets() {
        Assert.assertNotNull(twitterUser.getTweets());
    }

    @Test
    public void testSetTweets() {
        List<Tweet> listOfTweets = new ArrayList<>(twitterUser.getTweets());
        Tweet tweet1 = gd.generateTweet(twitterUser);
        twitterUser.getTweets().add(tweet1);
        
        Assert.assertNotEquals(listOfTweets, twitterUser.getTweets());
    }

    @Test
    public void testGetFollowing() {
        Assert.assertNotNull(twitterUser.getFollowing());
    }

    @Test
    public void testSetFollowing() {
        List<TwitterUser> listOfFollowing = new ArrayList<>(twitterUser.getFollowing());
        TwitterUser newUser = gd.generateUser();
        twitterUser.getFollowing().add(newUser);
        
        Assert.assertNotEquals(listOfFollowing, twitterUser.getFollowing());
    }

    @Test
    public void testGetUserRole() {
        Assert.assertNotNull(twitterUser.getUserRole());
    }

    @Test
    public void testSetUserRole() {
        UserRole role = twitterUser.getUserRole();
        twitterUser.setUserRole(UserRole.MODERATOR);
        Assert.assertNotEquals(role, twitterUser.getUserRole());
    }
}

package com.guido.models;

import com.guido.util.GenerateData;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Guido Thomassen
 */
public class TweetTest {

    private TwitterUser twitterUser;

    GenerateData gd = new GenerateData();

    @Before
    public void setup() {
        twitterUser = gd.generateUser();
    }

    @Test
    public void testGetId() {
        if (twitterUser.getTweets().size() <= 0) {
            fail("There are no tweets for this user");
        }

        Assert.assertNotNull(twitterUser.getTweets().iterator().next().getId());
    }

    @Test
    public void testSetId() {
        if (twitterUser.getTweets().size() <= 0) {
            fail("There are no tweets for this user");
        }

        Long previousID = twitterUser.getTweets().iterator().next().getId();
        twitterUser.getTweets().iterator().next().setId(new Long(-10000));
        Assert.assertNotEquals(previousID, twitterUser.getTweets().iterator().next().getId());
    }

    @Test
    public void testGetContent() {
        if (twitterUser.getTweets().size() <= 0) {
            fail("There are no tweets for this user");
        }

        Assert.assertNotNull(twitterUser.getTweets().iterator().next().getContent());
    }

    @Test
    public void testSetContent() {
        if (twitterUser.getTweets().size() <= 0) {
            fail("There are no tweets for this user");
        }

        String previousContent = twitterUser.getTweets().iterator().next().getContent();
        twitterUser.getTweets().iterator().next().setContent("NEW CONTENT");
        Assert.assertNotEquals(previousContent, twitterUser.getTweets().iterator().next().getContent());
    }

    @Test
    public void testGetOwner() {
        if (twitterUser.getTweets().size() <= 0) {
            fail("There are no tweets for this user");
        }

        Assert.assertNotNull(twitterUser.getTweets().iterator().next().getOwner());
    }

    @Test
    public void testSetOwner() {
        if (twitterUser.getTweets().size() <= 0) {
            fail("There are no tweets for this user");
        }

        TwitterUser newUser = gd.generateUser();
        Tweet tweet = twitterUser.getTweets().iterator().next();

        TwitterUser previousOwner = tweet.getOwner();
        tweet.setOwner(newUser);
        Assert.assertNotEquals(previousOwner, tweet.getOwner());
        Assert.assertTrue(newUser.getTweets().contains(tweet));
    }
}

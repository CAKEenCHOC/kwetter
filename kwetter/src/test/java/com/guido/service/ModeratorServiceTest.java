/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guido.service;

import com.guido.dao.implementations.ModeratorDaoImpl;
import com.guido.enums.UserRole;
import com.guido.models.TwitterUser;
import com.guido.services.ModeratorService;
import com.guido.util.GenerateData;
import java.util.HashSet;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Guido Thomassen
 */
public class ModeratorServiceTest {
    
    ModeratorService moderatorService;
    
    ModeratorDaoImpl moderatorDao;
    
    GenerateData gd = new GenerateData();
    
    @Before
    public void setup() {
        this.moderatorService = new ModeratorService();
        this.moderatorDao = mock(ModeratorDaoImpl.class);
        this.moderatorService.setDaoForTesting(moderatorDao);
        
        TwitterUser moderator = gd.generateUser();
        moderator.setUserRole(UserRole.MODERATOR);
        
        when(this.moderatorDao.changeModeratorToUser(1L)).thenReturn(moderator);
        
        TwitterUser normal = gd.generateUser();
        normal.setUserRole(UserRole.NORMAL);
        
        when(this.moderatorDao.changeUserToModerator(1L)).thenReturn(normal);
        
        HashSet<TwitterUser> users = new HashSet<>();
        users.add(normal);
        users.add(moderator);
        
        when(this.moderatorDao.getUsersWithRole()).thenReturn(new HashSet<>(users));
        when(this.moderatorDao.deleteTweet(1L)).thenReturn(true);
    }
    
    @Test
    public void changeUserToModeratorCorrect() {
        Assert.assertNotNull(this.moderatorService.changeModeratorToUser(1L));
    }
    
    @Test
    public void changeUserToModeratorNull() {
        Assert.assertNull(this.moderatorService.changeModeratorToUser(null));
    }
    
    @Test
    public void changeModeratorToUserCorrect() {
        Assert.assertNotNull(this.moderatorService.changeModeratorToUser(1L));
    }
    
    @Test
    public void changeModeratorToUserNull() {
        Assert.assertNull(this.moderatorService.changeModeratorToUser(null));
    }
    
    @Test
    public void deleteTweetCorrect() {
        Assert.assertTrue(this.moderatorService.deleteTweet(1L));
    }
    
    @Test
    public void deleteTweetNull() {
        Assert.assertFalse(this.moderatorService.deleteTweet(null));
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guido.service;

import com.guido.dao.implementations.UserDaoImpl;
import com.guido.models.Tweet;
import com.guido.models.TwitterUser;
import com.guido.services.UserService;
import com.guido.util.GenerateData;
import java.util.HashSet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Guido Thomassen
 */
public class UserServiceTest {
    
    UserService userService;
    
    UserDaoImpl userDao;
    
    GenerateData gd = new GenerateData();
    
    TwitterUser user;
    
    @Before
    public void setup() {
        this.userService = new UserService();
        this.userDao = mock(UserDaoImpl.class);
        this.userService.setDaoForTesting(userDao);
        
        user = gd.generateUser();
        when(this.userDao.changeUsername(1L, "Test")).thenReturn(user);
        when(this.userDao.changeWebsite(1L, "Test")).thenReturn(user);
        when(this.userDao.changeDescription(1L, "Test")).thenReturn(user);
        when(this.userDao.getFollowing(1L)).thenReturn(user.getFollowing());
        when(this.userDao.getFollowers(1L)).thenReturn(new HashSet<TwitterUser>());
        when(this.userDao.generateTimeline(1L)).thenReturn(new HashSet<Tweet>());
    }
    
    @Test
    public void testUserNameCorrect() {
        Assert.assertNotNull(this.userService.changeUsername(1L, "Test"));
    }
    
    @Test
    public void testUserNameNull() {
        Assert.assertNull(this.userService.changeUsername(1L, null));
    }
    
    @Test
    public void testUserNameEmpty() {
        Assert.assertNull(this.userService.changeUsername(1L, ""));
    }
    
    @Test
    public void testUserNameEmptySpaces() {
        Assert.assertNull(this.userService.changeUsername(1L, " "));
    }
    
    @Test
    public void testChangeDescriptionCorrect() {
        Assert.assertNotNull(this.userService.changeDescription(1L, "Test"));
    }
    
    @Test
    public void testChangeDescriptionNull() {
        Assert.assertNull(this.userService.changeDescription(1L, null));
    }
    
    @Test
    public void testChangeWebsiteCorrect() {
        Assert.assertNotNull(this.userService.changeWebsite(1L, "Test"));
    }
    
    @Test
    public void testChangeWebsiteNull() {
        Assert.assertNull(this.userService.changeWebsite(1L, null));
    }
    
    @Test
    public void testGetFollowingCorrect() {
        Assert.assertNotNull(this.userService.getFollowing(1L));
    }
    
    @Test
    public void testGetFollowingNull() {
        Assert.assertNull(this.userService.getFollowing(null));
    }
    
    @Test
    public void testGetFollowersCorrect() {
        Assert.assertNotNull(this.userService.getFollowers(1L));
    }
    
    @Test
    public void testGetFollowersNull() {
        Assert.assertNull(this.userService.getFollowers(null));
    }
    
    @Test
    public void testGenerateTimelineCorrect() {
        Assert.assertNotNull(this.userService.generateTimeline(1L));
    }
    
    @Test
    public void testGenerateTimelineNull() {
        Assert.assertNull(this.userService.generateTimeline(null));
    }
}

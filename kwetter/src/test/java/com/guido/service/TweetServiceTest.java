/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guido.service;

import com.guido.dao.implementations.TweetDaoImpl;
import com.guido.models.Tweet;
import com.guido.models.TwitterUser;
import com.guido.services.TweetService;
import com.guido.util.GenerateData;
import java.util.HashSet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Guido Thomassen
 */
public class TweetServiceTest {
    
    TweetService tweetService;
    
    TweetDaoImpl tweetDao;
    
    GenerateData gd = new GenerateData();
    
    TwitterUser user;
    Tweet tweet;
    
    @Before
    public void setup() {
        this.tweetService = new TweetService();
        this.tweetDao = mock(TweetDaoImpl.class);
        this.tweetService.setDaoForTesting(tweetDao);
        
        user = gd.generateUser();
        tweet = user.getTweets().iterator().next();
        
        when(tweetDao.getLastTweets(10, 1L)).thenReturn(user.getTweets());
        when(tweetDao.retweet(1L, 2L)).thenReturn(tweet);
        when(tweetDao.getTweetWithCriteria("something")).thenReturn(new HashSet<Tweet>());
        when(tweetDao.publishTweet(1L, "content")).thenReturn(new Tweet());
    }
    
    @Test
    public void getLastTweetsCorrect() {
        Assert.assertNotNull(tweetService.getLastTweets(10, 1L));
    }
    
    @Test
    public void getLastTweetsNegative() {
        Assert.assertNull(tweetService.getLastTweets(-1, 1L));
    }
    
    @Test
    public void getLastTweetsNull() {
        Assert.assertNull(tweetService.getLastTweets(10, null));
    }
    
    @Test
    public void retweetCorrect() {
        Assert.assertNotNull(tweetService.retweet(1L, 2L));
    }
    
    @Test
    public void retweetNullTweet() {
        Assert.assertNull(tweetService.retweet(null, 2L));
    }
    
    @Test
    public void retweetNullUser() {
        Assert.assertNull(tweetService.retweet(1L, null));
    }
    
    @Test
    public void searchCorrect() {
        Assert.assertNotNull(tweetService.getTweetWithCriteria("something"));
    }
    
    @Test
    public void searchNull() {
        Assert.assertNull(tweetService.getTweetWithCriteria(null));
    }
    
    @Test
    public void searchSpaces() {
        Assert.assertNull(tweetService.getTweetWithCriteria(" "));
    }
    
    @Test
    public void searchEmpty() {
        Assert.assertNull(tweetService.getTweetWithCriteria(""));
    }
    
    @Test
    public void publishTweetCorrect() {
        Assert.assertNotNull(tweetService.publishTweet(1L, "content"));
    }
    
    @Test
    public void publishTweetNull() {
        Assert.assertNull(tweetService.publishTweet(null, "content"));
    }
    
    @Test
    public void publishTweetContentNull() {
        Assert.assertNull(tweetService.publishTweet(1L, null));
    }
    
    @Test
    public void publishTweetContentEmpty() {
        Assert.assertNull(tweetService.publishTweet(1L, ""));
    }
    
    @Test
    public void publishTweetContentSpaces() {
        Assert.assertNull(tweetService.publishTweet(1L, " "));
    }
}

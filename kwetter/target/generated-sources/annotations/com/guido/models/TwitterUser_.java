package com.guido.models;

import com.guido.enums.UserRole;
import com.guido.models.Tweet;
import com.guido.models.TwitterUser;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-03-08T22:08:41")
@StaticMetamodel(TwitterUser.class)
public class TwitterUser_ { 

    public static volatile SingularAttribute<TwitterUser, String> website;
    public static volatile SetAttribute<TwitterUser, TwitterUser> following;
    public static volatile SingularAttribute<TwitterUser, String> description;
    public static volatile SingularAttribute<TwitterUser, String> location;
    public static volatile SingularAttribute<TwitterUser, Long> id;
    public static volatile SingularAttribute<TwitterUser, UserRole> userRole;
    public static volatile SetAttribute<TwitterUser, Tweet> tweets;
    public static volatile SingularAttribute<TwitterUser, String> username;

}